import { Component } from '@angular/core';

declare var webview: any;
declare var device: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CordovaMobileAppWeb';
  version = device.version;
  manufacturer = device.manufacturer;

  switch() {
    webview.Close();
  }

}
